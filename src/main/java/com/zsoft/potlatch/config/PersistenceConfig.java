package com.zsoft.potlatch.config;

import java.net.URI;
import java.net.URISyntaxException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@Profile("heroku")
public class PersistenceConfig {

    @Value("${heroku.postgres.username}")
    private String username;
    
    @Value("${heroku.postgres.password}")
    private String password;

    @Value("${heroku.postgres.databaseurl}")
    private String url;

    @Bean
    public DataSource restDataSource() throws URISyntaxException {
        URI dbUri;
        try {
            String dbUrlEnv = System.getenv("DATABASE_URL");
            if (dbUrlEnv != null) {
                dbUri = new URI(dbUrlEnv);

                username = dbUri.getUserInfo().split(":")[0];
                password = dbUri.getUserInfo().split(":")[1];
                url = "jdbc:postgresql://" + dbUri.getHost() + dbUri.getPath();
            }

            DriverManagerDataSource basicDataSource = new DriverManagerDataSource();
            basicDataSource.setDriverClassName("org.postgresql.Driver");
            basicDataSource.setPassword(password);
            basicDataSource.setUsername(username);
            basicDataSource.setUrl(url);
            return basicDataSource;

        } catch (URISyntaxException e) {
            //Deal with errors here.
            throw e;
        }
    }

  
}

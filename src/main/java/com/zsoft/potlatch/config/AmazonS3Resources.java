package com.zsoft.potlatch.config;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;

import java.io.InputStream;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.io.FilenameUtils;

public class AmazonS3Resources {

    protected static final Logger logger = 
            Logger.getLogger(AmazonS3Resources.class.getName());
    
    protected static final String imageUrlTemplate = 
            "https://s3-us-west-2.amazonaws.com/{0}/{1}";
    
    protected static final Set<String> permittedExtensions = 
            new HashSet<>();
        
    private final String bucketName;
    private final AmazonS3Client amazonS3Client;

    public AmazonS3Resources(AmazonS3Client amazonS3Client, String bucketName) 
            throws AmazonServiceException {

        this.amazonS3Client = amazonS3Client;
        
        if (!amazonS3Client.doesBucketExist(bucketName)) {
            throw new IllegalArgumentException("Bucket " + bucketName + " does not exist");
        }
        
        this.bucketName = bucketName;
        
        permittedExtensions.add("jpg");
        permittedExtensions.add("png");
        permittedExtensions.add("gif");
    }

    /**
     * Upload an image to Amazon S3
     *
     * @param in
     * @param objectMetadata
     * @param fileName
     * @return uploaded image URL
     * @throws AmazonServiceException
     * @throws IllegalArgumentException
     */
    public String uploadImage(InputStream in, ObjectMetadata objectMetadata, String fileName) 
            throws AmazonServiceException, IllegalArgumentException {

        String fileExtension = FilenameUtils.getExtension(fileName);
        if (fileExtension == null || fileExtension.isEmpty()) {
            throw new IllegalArgumentException(
                    "Invalid filename without extension: " + fileName);
        }
        
        fileExtension = fileExtension.toLowerCase();

        if (!permittedExtensions.contains(fileExtension)) {
            throw new IllegalArgumentException(
                    "Invalid file extension '" + fileExtension + "' in " + fileName);
        }
        
        String imageFileName = MessageFormat.format(
                "{0}.{1}", UUID.randomUUID().toString(), fileExtension);

        amazonS3Client.putObject(bucketName, imageFileName, in, objectMetadata);

        String imageUrl = MessageFormat.format(
                imageUrlTemplate, bucketName, imageFileName);
        
        logger.log(Level.INFO, "Image uploaded to Amazon S3: {0}", imageUrl);

        return imageUrl;
    }

    public void deleteImage(String url) {
        String imageName = FilenameUtils.getName(url);
        amazonS3Client.deleteObject(bucketName, imageName);
    }
}
package com.zsoft.potlatch.service;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.zsoft.potlatch.domain.UserDao;

import java.util.List;

public interface UserRepository extends PagingAndSortingRepository<UserDao, Long> {
    UserDao findById(Long id);
    UserDao findByUsernameIgnoreCase(String username);
    UserDao findByEmailIgnoreCase(String email);
    
    UserDao findByUsernameIgnoreCaseOrEmailIgnoreCase(String username, String email);
    
    List<UserDao> findByUsernameContainsIgnoreCase(String usernamePart, Pageable pageable);
}

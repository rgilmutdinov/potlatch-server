package com.zsoft.potlatch.service;

import com.zsoft.potlatch.domain.VoteDao;
import org.springframework.data.repository.CrudRepository;

public interface VoteRepository extends CrudRepository<VoteDao, Long> {
    VoteDao findByGiftIdAndUserId(long giftId, long userId);
}

package com.zsoft.potlatch.service;

import com.zsoft.potlatch.domain.VoteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VoteServiceImpl implements VoteService {
    @Autowired
    private VoteRepository voteRepository;
    
    @Override
    public VoteDao findByGiftIdAndUserId(long giftId, long userId) {
        return voteRepository.findByGiftIdAndUserId(giftId, userId);
    
    }
    
    @Override
    public VoteDao save(VoteDao vote) {
        return voteRepository.save(vote);
    }
    
    @Override
    public void delete(Long id) {
        voteRepository.delete(id);
    }
}
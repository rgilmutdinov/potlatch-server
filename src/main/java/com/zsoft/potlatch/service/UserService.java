package com.zsoft.potlatch.service;

import com.zsoft.potlatch.domain.UserDao;

import java.util.List;

import org.springframework.data.domain.Pageable;

public interface UserService {
    UserDao save(UserDao user);
    void delete(Long id);
    
    UserDao findByUsername(String username);
    UserDao findByEmail(String email);
    UserDao findById(Long id);
    UserDao findByUsernameOrEmail(String username, String email);
    
    List<UserDao> findByIds(Iterable<Long> ids);
    List<UserDao> getAllUsers(Pageable pageable);
    List<UserDao> searchUsers(String query, Pageable pageable);
}

package com.zsoft.potlatch.service;

import com.zsoft.potlatch.domain.VoteDao;

public interface VoteService {
    VoteDao findByGiftIdAndUserId(long giftId, long userId);
    VoteDao save(VoteDao vote);
    void delete(Long id);
}
package com.zsoft.potlatch.service;

import com.zsoft.potlatch.domain.GiftDao;

import java.util.List;

import org.springframework.data.domain.Pageable;

public interface GiftService {
    boolean exists(Long id);
    void delete(Long id);
    
    GiftDao save(GiftDao gift);
    GiftDao findById(Long id);
    GiftDao findOne(Long id);
    
    List<GiftDao> findByIds(Iterable<Long> ids);
    
    List<GiftDao> findAll(Boolean includeObscene, Pageable pageable);
    List<GiftDao> findByOwnerId(Long ownerId, Boolean includeObscene);
    List<GiftDao> findByOwnerId(Long ownerId, Boolean includeObscene, Pageable pageable);
    
    List<GiftDao> findByParentId(Long parentId);
    List<GiftDao> findByParentIdIsNull(Boolean includeObscene, Pageable pageable);
    List<GiftDao> findByParentId(Long parentId, Boolean includeObscene, Pageable pageable);
    
    List<GiftDao> searchGifts(String query, Boolean includeObscene, Pageable pageable);
    List<GiftDao> searchGiftsOfOwner(Long ownerId, String query, Boolean includeObscene, Pageable pageable);
    List<GiftDao> searchChainedGifts(Long parentId, String query, Boolean includeObscene, Pageable pageable);
}
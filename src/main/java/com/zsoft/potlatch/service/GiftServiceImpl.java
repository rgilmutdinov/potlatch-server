package com.zsoft.potlatch.service;

import com.google.common.collect.Lists;
import com.zsoft.potlatch.domain.GiftDao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class GiftServiceImpl implements GiftService {
    @Autowired
    private GiftRepository giftRepository;

    @Override
    public GiftDao save(GiftDao gift) {
        return giftRepository.save(gift);
    }

    @Override
    public void delete(Long id) {
        giftRepository.delete(id);
    }

    @Override
    public GiftDao findById(Long id) {
        return giftRepository.findOne(id);
    }

    @Override
    public List<GiftDao> findAll(Boolean includeObscene, Pageable pageable) {
        if (includeObscene == null || includeObscene) {
            return Lists.newArrayList(giftRepository.findAll(pageable));
        }
        return Lists.newArrayList(giftRepository.findByRatingGreaterThanEqual(0L, pageable));
    }

    @Override
    public List<GiftDao> findByOwnerId(Long ownerId, Boolean includeObscene) {
        if (includeObscene == null || includeObscene) {
            return Lists.newArrayList(giftRepository.findByOwnerId(ownerId));
        }
        return Lists.newArrayList(giftRepository.findByOwnerIdAndRatingGreaterThanEqual(ownerId, 0L));
    }

    @Override
    public List<GiftDao> findByParentId(Long parentId) {
        return Lists.newArrayList(giftRepository.findByParentId(parentId));
    }
    
    @Override 
    public List<GiftDao> findByParentIdIsNull(Boolean includeObscene, Pageable pageable) {
        if (includeObscene == null || includeObscene) {
            return Lists.newArrayList(giftRepository.findByParentIdIsNull(pageable));
        }
        return Lists.newArrayList(giftRepository.findByParentIdIsNullAndRatingGreaterThanEqual(0L, pageable));
    }
    
    @Override 
    public boolean exists(Long id) {
        return giftRepository.exists(id);
    }
    
    @Override 
    public GiftDao findOne(Long id) {
        return giftRepository.findOne(id);
    }

    @Override
    public List<GiftDao> findByParentId(Long parentId, Boolean includeObscene, Pageable pageable) {
        if (includeObscene == null || includeObscene) {
            return Lists.newArrayList(giftRepository.findByParentId(parentId, pageable));
        }
        return Lists.newArrayList(giftRepository.findByParentIdAndRatingGreaterThanEqual(parentId, 0L, pageable));
    }

    @Override
    public List<GiftDao> searchGifts(String query, Boolean includeObscene, Pageable pageable) {
        if (includeObscene == null || includeObscene) {
            return Lists.newArrayList(giftRepository
                .findByParentIdIsNullAndTitleContainsIgnoreCase(query, pageable));
        }
        return Lists.newArrayList(giftRepository
                .findByParentIdIsNullAndTitleContainsIgnoreCaseAndRatingGreaterThanEqual(query, 0L, pageable));
    }

    @Override
    public List<GiftDao> searchChainedGifts(Long parentId, String query, Boolean includeObscene,
            Pageable pageable) {
        if (includeObscene == null || includeObscene) {
            return Lists.newArrayList(giftRepository
                .findByParentIdAndTitleContainsIgnoreCase(parentId, query, pageable));
        }
        return Lists.newArrayList(giftRepository
                .findByParentIdAndTitleContainsIgnoreCaseAndRatingGreaterThanEqual(parentId, query, 0L, pageable));
    }

    @Override
    public List<GiftDao> findByOwnerId(Long ownerId, Boolean includeObscene, Pageable pageable) {
        if (includeObscene == null || includeObscene) {
            return Lists.newArrayList(giftRepository.findByOwnerId(ownerId, pageable));
        }
        return Lists.newArrayList(giftRepository.findByOwnerIdAndRatingGreaterThanEqual(ownerId, 0L, pageable));
    }

    @Override
    public List<GiftDao> searchGiftsOfOwner(Long ownerId, String query, Boolean includeObscene,
            Pageable pageable) {
        if (includeObscene == null || includeObscene) {
            return Lists.newArrayList(giftRepository
                .findByOwnerIdAndTitleContainsIgnoreCase(ownerId, query, pageable));
        }
        return Lists.newArrayList(giftRepository
                .findByOwnerIdAndTitleContainsIgnoreCaseAndRatingGreaterThanEqual(ownerId, query, 0L, pageable));
    }

    @Override
    public List<GiftDao> findByIds(Iterable<Long> ids) {
        return Lists.newArrayList(giftRepository.findAll(ids));
    }
}

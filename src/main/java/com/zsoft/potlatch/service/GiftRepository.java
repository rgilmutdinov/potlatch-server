package com.zsoft.potlatch.service;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.zsoft.potlatch.domain.GiftDao;

import java.util.List;

import org.springframework.data.domain.Pageable;

public interface GiftRepository extends PagingAndSortingRepository<GiftDao, Long> {    
    List<GiftDao> findByParentId(Long parentId);
    
    List<GiftDao> findByParentIdIsNull(Pageable pageable);
    List<GiftDao> findByParentId(Long parentId, Pageable pageable);
    List<GiftDao> findByParentIdIsNullAndTitleContainsIgnoreCase(String title, Pageable pageable);
    List<GiftDao> findByParentIdAndTitleContainsIgnoreCase(Long parentId, String title, Pageable pageable);
    
    List<GiftDao> findByParentIdIsNullAndRatingGreaterThanEqual(Long rating, Pageable pageable);
    List<GiftDao> findByParentIdAndRatingGreaterThanEqual(Long parentId, Long rating, Pageable pageable);
    List<GiftDao> findByParentIdIsNullAndTitleContainsIgnoreCaseAndRatingGreaterThanEqual(String title, Long rating, Pageable pageable);
    List<GiftDao> findByParentIdAndTitleContainsIgnoreCaseAndRatingGreaterThanEqual(Long parentId, String title, Long rating, Pageable pageable);
    
    List<GiftDao> findByOwnerId(Long ownerId);
    List<GiftDao> findByOwnerIdAndRatingGreaterThanEqual(Long ownerId, Long rating);
    
    List<GiftDao> findByOwnerId(Long ownerId, Pageable pageable);
    List<GiftDao> findByOwnerIdAndTitleContainsIgnoreCase(Long ownerId, String title, Pageable pageable);
    
    List<GiftDao> findByOwnerIdAndRatingGreaterThanEqual(Long ownerId, Long rating, Pageable pageable);
    List<GiftDao> findByOwnerIdAndTitleContainsIgnoreCaseAndRatingGreaterThanEqual(Long ownerId, String title, Long rating, Pageable pageable);

    List<GiftDao> findByRatingGreaterThanEqual(Long rating, Pageable pageable);
}
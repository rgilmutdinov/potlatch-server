package com.zsoft.potlatch.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.zsoft.potlatch.domain.UserDao;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDao findByUsername(String username) {
        return userRepository.findByUsernameIgnoreCase(username);
    }

    @Override
    public UserDao findById(Long id) {
        return userRepository.findById(id);
    }
    
    @Override
    public UserDao findByEmail(String email) {
        return userRepository.findByEmailIgnoreCase(email);
    }
    
    @Override
    public UserDao save(UserDao user) {
        return userRepository.save(user);
    }

    @Override
    public void delete(Long id) {
        userRepository.delete(id);        
    }

    @Override
    public UserDao findByUsernameOrEmail(String username, String email) {
        return userRepository.findByUsernameIgnoreCaseOrEmailIgnoreCase(username, email);
    }

    @Override
    public List<UserDao> getAllUsers(Pageable pageable) {
        return Lists.newArrayList(userRepository.findAll(pageable));
    }

    @Override
    public List<UserDao> searchUsers(String usernamePart, Pageable pageable) {
        return Lists.newArrayList(userRepository.findByUsernameContainsIgnoreCase(usernamePart, pageable));
    }

    @Override
    public List<UserDao> findByIds(Iterable<Long> ids) {
        return Lists.newArrayList(userRepository.findAll(ids));
    }
}

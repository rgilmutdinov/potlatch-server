package com.zsoft.potlatch.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;
import java.util.TreeSet;
import javax.persistence.CascadeType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.core.style.ToStringCreator;
import org.springframework.data.rest.core.annotation.RestResource;

@Entity
@Table(name = "gift")
@RestResource(exported = false)
public class GiftDao extends DaoBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonIgnore
    @ManyToOne(optional = true)    
    private UserDao owner;

    @Column(name = "parent_id", nullable = true)
    private Long parentId;

    @Column(name = "title", length = 50)
    private String title;

    @Column(name = "text", nullable = true, length = 500)
    private String text;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false)
    private Calendar created;

    @Column(name = "url", nullable = false, length = 200)
    private String url;
    
    @Column(name = "views")
    private long views = 0;
    
    @Column(name = "rating")
    private long rating = 0;    
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gift", fetch = FetchType.LAZY)
    private Set<VoteDao> votes;
    
    protected GiftDao() {
        votes = new TreeSet<>();
    }

    public GiftDao(UserDao owner, String title, String text) {
        super();
        
        this.owner = owner;
        this.title = title;
        this.text  = text;
        
        // set current date
        this.created = Calendar.getInstance();                
    }

    public GiftDao(UserDao owner, String title, String text, Long parentId) {
        this(owner, title, text);

        this.parentId = parentId;
    }

    public Long getParentId() {
        return parentId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    public Calendar getCreated() {
        return created;
    }

    public UserDao getOwner() {
        return owner;
    }

    public void setOwner(UserDao owner) {
        this.owner = owner;
    }
    
    public long getRating() {
        return rating;
    }
    
    public void setRating(long rating) {
        this.rating = rating;
    }
    
    public long getViews() {
        return views;
    }
    
    public void setViews(long views) {
        this.views = views;
    }
    
    public Set<VoteDao> getVotes() {
        return votes;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(this.parentId)
                .append(this.title)
                .append(this.text)
                .append(this.created)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GiftDao other = (GiftDao) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.parentId, other.parentId)
                .append(this.title, other.title)
                .append(this.text, other.text)
                .append(this.created, other.created)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", this.getId())
                .append("parentId", this.parentId)
                .append("title", this.title)
                .append("text", this.text)
                .append("created", this.created)
                .append("url", this.url)
                .toString();
    }
}
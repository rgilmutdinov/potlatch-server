package com.zsoft.potlatch.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.core.style.ToStringCreator;
import org.springframework.data.rest.core.annotation.RestResource;

@Entity
@Table(name = "vote")
@RestResource(exported = false)
public class VoteDao extends DaoBase implements Serializable {
    public static final int VOTE_UP     = 1;
    public static final int VOTE_ABSENT = 0;
    public static final int VOTE_DOWN   = -1;

    private static final long serialVersionUID = 1L;

    public static VoteDao createVoteUp(GiftDao gift, Long userId) {
        return new VoteDao(gift, userId, VOTE_UP);
    }

    public static VoteDao createVoteDown(GiftDao gift, Long userId) {
        return new VoteDao(gift, userId, VOTE_DOWN);
    }

    @ManyToOne
    @JoinColumn(name = "gift_id", nullable = false)
    private GiftDao gift;

    @Column(name = "vote")
    private int vote = 0;
    
    @Column(name = "user_id", nullable = false)
    private Long userId;

    public VoteDao() {
        
    }
    
    public VoteDao(GiftDao gift, Long userId, int vote) {
        this.gift   = gift;
        this.userId = userId;
        this.vote   = vote;
    }

    public GiftDao getGift() {
        return gift;
    }

    public void setGift(GiftDao gift) {
        this.gift = gift;
    }

    public int getVote() {
        return vote;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }    
    
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(gift.hashCode())
                .append(this.vote)                
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VoteDao other = (VoteDao) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.gift, other.gift)
                .append(this.vote, other.vote)                
                .append(this.userId, other.userId)     
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", this.getId())
                .append("gift_id", this.gift.getId())
                .append("gift_title", this.gift.getTitle())
                .append("userId", this.userId)
                .append("vote", this.vote)                
                .toString();
    }
}
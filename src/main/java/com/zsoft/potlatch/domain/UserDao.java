package com.zsoft.potlatch.domain;

import java.util.Calendar;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.core.style.ToStringCreator;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "app_user")
@RestResource(exported = false)
public class UserDao extends DaoBase implements UserDetails {
    private static final long serialVersionUID = 1L;
    
    @Column(name = "username", nullable = false, unique = true, length = 50)
    private String username;

    @Column(name = "password", nullable = false, length = 50)
    private String password;

    @Column(name = "email", nullable = false, unique = true, length = 50)
    private String email;

    @Column(name = "user_role", nullable = false, length = 10)
    private String role;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "registration_date", nullable = false)
    private Calendar registrationDate;
    
    @Column(name = "rating")
    private long rating = 0;
    
    @Column(name = "gifts_posted")
    private long giftsPosted = 0;
     
    public static UserDao createUser(String name, String password, String email) {
        return new UserDao(name, password, email, Role.USER);
    }

    public static UserDao createAdmin(String name, String password, String email) {
        return new UserDao(name, password, email, Role.ADMIN);
    }

    protected UserDao() {

    }

    public UserDao(String username, String password, String email, String role) {
        this.username = username;
        this.password = password;
        this.email    = email;
        this.role     = role;

        this.registrationDate = Calendar.getInstance();
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
    
    public Calendar getRegistrationDate() {
        return registrationDate;
    }
    
    public String getRole() {
        return role;
    }

    public long getRating() {
        return rating;
    }

    public long getGiftsPosted() {
        return giftsPosted;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setRegistrationDate(Calendar registrationDate) {
        this.registrationDate = registrationDate;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }
    
    public void setGiftsPosted(long giftsPosted) {
        this.giftsPosted = giftsPosted;
    }
    
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.createAuthorityList(getRole());
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .appendSuper(super.hashCode())
                .append(this.username)
                .append(this.password)
                .append(this.email)
                .append(this.role)
                .append(this.registrationDate)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserDao other = (UserDao) obj;
        return new EqualsBuilder()
                .appendSuper(super.equals(obj))
                .append(this.username, other.username)
                .append(this.password, other.password)
                .append(this.email, other.email)
                .append(this.role, other.role)
                .append(this.registrationDate, other.registrationDate)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", this.getId())
                .append("username", this.username)
                .append("password", this.password)
                .append("email", this.email)
                .append("role", this.role)
                .append("registrationDate", this.registrationDate)
                .toString();
    }
}

package com.zsoft.potlatch.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Splitter;
import com.zsoft.potlatch.domain.GiftDao;
import com.zsoft.potlatch.domain.UserDao;
import com.zsoft.potlatch.domain.VoteDao;
import com.zsoft.potlatch.rest.domain.Gift;
import com.zsoft.potlatch.service.GiftService;
import com.zsoft.potlatch.service.VoteService;
import com.zsoft.potlatch.utils.SortUtils;

@RestController
public class GiftQueriesController {
    @Autowired
    private GiftService giftService;
    
    @Autowired
    private VoteService voteService;
    
    @RequestMapping(method = RequestMethod.GET, value = "/gifts/search")
    @ResponseStatus(HttpStatus.OK)
    public List<Gift> searchGifts(
            @RequestParam("q") String query,
            @RequestParam(value = "s",   defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d",   defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p",   defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l",   defaultValue = "10",   required = false) Integer size,
            @RequestParam(value = "obs", defaultValue = "true", required = false) Boolean includeObscene) {
                
        List<Gift> giftResponses = new ArrayList<>();
        Sort sort = SortUtils.getSort(sortField, direction);
        PageRequest pageRequest = new PageRequest(page, size, sort);   
        for (GiftDao giftDao : giftService.searchGifts(query, includeObscene, pageRequest)) {
            Gift gift = Gift.fromGift(giftDao);
            giftResponses.add(gift);
        }        
        
        return giftResponses;
    }   
    
    @RequestMapping(method = RequestMethod.GET, value = "/gifts/{id}/search")
    @ResponseStatus(HttpStatus.OK)
    public List<Gift> searchChainedGifts(
            @PathVariable Long id,
            @RequestParam("q") String query,
            @RequestParam(value = "s",   defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d",   defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p",   defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l",   defaultValue = "10",   required = false) Integer size,
            @RequestParam(value = "obs", defaultValue = "true", required = false) Boolean includeObscene) {

        List<Gift> giftResponses = new ArrayList<>();
        Sort sort = SortUtils.getSort(sortField, direction);
        PageRequest pageRequest = new PageRequest(page, size, sort);
        for (GiftDao gift : giftService.searchChainedGifts(id, query, includeObscene, pageRequest)) {
            giftResponses.add(Gift.fromGift(gift));
        }        
        
        return giftResponses;
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/gifts")
    @ResponseStatus(HttpStatus.OK)
    public List<Gift> getAllGifts(
            @RequestParam(value = "s",   defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d",   defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p",   defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l",   defaultValue = "10",   required = false) Integer size,
            @RequestParam(value = "obs", defaultValue = "true", required = false) Boolean includeObscene) {

        List<Gift> giftResponses = new ArrayList<>();
        Sort sort = SortUtils.getSort(sortField, direction);
        PageRequest pageRequest = new PageRequest(page, size, sort);
        for (GiftDao gift : giftService.findByParentIdIsNull(includeObscene, pageRequest)) {
            giftResponses.add(Gift.fromGift(gift));
        }        
        
        return giftResponses;
    }
        
    @RequestMapping(method = RequestMethod.GET, value = "/gifts/{id}/chain")
    @ResponseStatus(HttpStatus.OK)
    public List<Gift> getChainedGifts(
            @PathVariable Long id,
            @RequestParam(value = "s",   defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d",   defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p",   defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l",   defaultValue = "10",   required = false) Integer size,
            @RequestParam(value = "obs", defaultValue = "true", required = false) Boolean includeObscene) {

        List<Gift> giftResponses = new ArrayList<>();
        Sort sort = SortUtils.getSort(sortField, direction);
        PageRequest pageRequest = new PageRequest(page, size, sort);
        for (GiftDao gift : giftService.findByParentId(id, includeObscene, pageRequest)) {
            giftResponses.add(Gift.fromGift(gift));
        }        
        
        return giftResponses;
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/gifts/{id}")
    public ResponseEntity<Gift> viewGift(
            @PathVariable Long id, 
            Authentication authentication) {
        GiftDao gift = giftService.findById(id);
        if (gift == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        // increase number of views
        gift.setViews(gift.getViews() + 1);
        giftService.save(gift);
        
        Gift giftResponse = Gift.fromGift(gift);
        UserDao user = (UserDao) authentication.getPrincipal();
        VoteDao vote = voteService.findByGiftIdAndUserId(id, user.getId());
        if (vote != null) {
            giftResponse.setVote(vote.getVote());
        }
        
        return new ResponseEntity<>(giftResponse, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/users/{ownerId}/gifts")
    @ResponseStatus(HttpStatus.OK)
    public List<Gift> getAllGiftsOfUser(
            @PathVariable Long ownerId, 
            @RequestParam(value = "s",   defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d",   defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p",   defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l",   defaultValue = "10",   required = false) Integer size,
            @RequestParam(value = "obs", defaultValue = "true", required = false) Boolean includeObscene) {
        return getAllGiftsOfUserImpl(ownerId, sortField, direction, includeObscene, page, size);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/users/{ownerId}/gifts/search")
    @ResponseStatus(HttpStatus.OK)
    public List<Gift> searchGiftsOfUser(
            @PathVariable Long ownerId, 
            @RequestParam("q") String query,
            @RequestParam(value = "s",   defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d",   defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p",   defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l",   defaultValue = "10",   required = false) Integer size,
            @RequestParam(value = "obs", defaultValue = "true", required = false) Boolean includeObscene) {
        return searchGiftsOfUserImpl(ownerId, query, sortField, direction, includeObscene, page, size);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/users/me/gifts")
    @ResponseStatus(HttpStatus.OK)
    public List<Gift> getMyGifts(
            @RequestParam(value = "s", defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d", defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p", defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l", defaultValue = "10",   required = false) Integer size,
            Authentication authentication) {
        UserDao user = (UserDao) authentication.getPrincipal();
        return getAllGiftsOfUserImpl(user.getId(), sortField, direction, true, page, size);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/users/me/gifts/search")
    @ResponseStatus(HttpStatus.OK)
    public List<Gift> searchMyGifts(
            @RequestParam("q") String query,
            @RequestParam(value = "s", defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d", defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p", defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l", defaultValue = "10",   required = false) Integer size,
            Authentication authentication) {
        UserDao user = (UserDao) authentication.getPrincipal();
        return searchGiftsOfUserImpl(user.getId(), query, sortField, direction, true, page, size);
    }
    
    private List<Gift> getAllGiftsOfUserImpl(
            Long userId, String sortField, String direction, Boolean includeObscene, Integer page, Integer size) {
        List<Gift> giftResponses = new ArrayList<>();
        Sort sort = SortUtils.getSort(sortField, direction);
        PageRequest pageRequest = new PageRequest(page, size, sort);
        for (GiftDao gift : giftService.findByOwnerId(userId, includeObscene, pageRequest)) {
            giftResponses.add(Gift.fromGift(gift));
        }        
        
        return giftResponses;
    }
    
    private List<Gift> searchGiftsOfUserImpl(
            Long userId, String query, String sortField, String direction, Boolean includeObscene, Integer page, Integer size) {
        List<Gift> giftResponses = new ArrayList<>();
        Sort sort = SortUtils.getSort(sortField, direction);
        PageRequest pageRequest = new PageRequest(page, size, sort);        
        for (GiftDao gift : giftService.searchGiftsOfOwner(userId, query, includeObscene, pageRequest)) {
            giftResponses.add(Gift.fromGift(gift));
        }        
        
        return giftResponses;
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/gifts/refresh/{ids}")
    @ResponseStatus(HttpStatus.OK)
    public List<Gift> updateGiftsCounts(@PathVariable String ids) {
        List<Gift> gifts = new ArrayList<>();
        if (ids == null || ids.isEmpty()) {
            return gifts;
        }
        
        List<Long> idList = new ArrayList<>();
        for (String sid : Splitter.on(',').split(ids)) {
            try {
                Long id = Long.parseLong(sid);
                idList.add(id);
            } catch (Exception exc) {
                /* do nothing, just skip */
            }
        }
        
        if (idList.size() == 0) {
            return gifts;
        }
        
        List<GiftDao> giftDaos = giftService.findByIds(idList);
        for (GiftDao giftDao : giftDaos) {
            gifts.add(Gift.fromGift(giftDao));
        }
        return gifts;
    }
}
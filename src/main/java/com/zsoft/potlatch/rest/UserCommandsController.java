package com.zsoft.potlatch.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.zsoft.potlatch.domain.UserDao;
import com.zsoft.potlatch.rest.domain.UserForm;
import com.zsoft.potlatch.rest.domain.User;
import com.zsoft.potlatch.service.UserService;

@RestController
@RequestMapping("/users")
public class UserCommandsController {
    @Autowired
    private UserService userService;
    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> registerUser(@RequestBody UserForm userForm) {
        if (userService.findByUsernameOrEmail(
                userForm.getUsername(), userForm.getEmail()) != null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        
        UserDao userToSave = UserForm.toUser(userForm);
        UserDao user = userService.save(userToSave);
        User userResponse = User.fromUser(user);
        return new ResponseEntity<>(userResponse, HttpStatus.CREATED);
    }
}

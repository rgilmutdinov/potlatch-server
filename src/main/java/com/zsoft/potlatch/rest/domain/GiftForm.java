package com.zsoft.potlatch.rest.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.core.style.ToStringCreator;

import com.zsoft.potlatch.domain.GiftDao;
import com.zsoft.potlatch.domain.UserDao;

public class GiftForm {
    private Long parentId;

    private String title;
    private String text;
    
    public static GiftDao toGift(UserDao owner, GiftForm giftForm) {
        return new GiftDao(owner, giftForm.title, giftForm.text, giftForm.parentId);
    }

    public static GiftForm fromGift(GiftDao gift) {
        return new GiftForm(
                gift.getParentId(), 
                gift.getTitle(), 
                gift.getText());
    }
    
    protected GiftForm() {
        
    }

    public GiftForm(Long parentId, String title, String text) {
        this.parentId = parentId;
        this.title    = title;
        this.text     = text;
    }
    
    public GiftDao toGift(UserDao owner) {
        return toGift(owner, this);
    }
    
    public Long getParentId() {
        return parentId;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.parentId)
                .append(this.title)
                .append(this.text)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GiftForm other = (GiftForm) obj;
        return new EqualsBuilder()
                .append(this.parentId, other.parentId)
                .append(this.title, other.title)
                .append(this.text, other.text)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("parentId", this.parentId)
                .append("title", this.title)
                .append("text", this.text)
                .toString();
    }
}

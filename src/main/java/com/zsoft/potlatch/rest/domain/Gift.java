package com.zsoft.potlatch.rest.domain;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.core.style.ToStringCreator;

import com.zsoft.potlatch.domain.GiftDao;
import com.zsoft.potlatch.domain.UserDao;

public class Gift {
    private Long id;
    private Long parentId;

    private String title;
    private String text;
    
    private Calendar created;
    private String url;
    
    private long views;
    private long rating;
    private long age = 0;
    
    private String ownerName;
    private Long ownerId;
    
    private int vote;
    
    public static Gift fromGift(GiftDao gift) {
        return new Gift(gift);
    }       
        
    public Gift() {
        
    }
    
    private Gift(GiftDao gift) {        
        this.id       = gift.getId();
        this.parentId = gift.getParentId();
        this.title    = gift.getTitle();
        this.text     = gift.getText();
        this.created  = gift.getCreated();
        this.url      = gift.getUrl();
        this.rating   = gift.getRating();
        this.views    = gift.getViews();
        
        UserDao owner  = gift.getOwner();
        this.ownerName = owner.getUsername();
        this.ownerId   = owner.getId();
        this.vote      = 0;
        
        if (created != null) {
            this.age = new Date().getTime() - this.created.getTimeInMillis();
        }
    }
    
    public Long getId() {
        return id;
    }

    public Long getParentId() {
        return parentId;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public Calendar getCreated() {
        return created;
    }

    public String getUrl() {
        return url;
    }

    public long getViews() {
        return views;
    }

    public long getRating() {
        return rating;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setCreated(Calendar created) {
        this.created = created;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setViews(long views) {
        this.views = views;
    }

    public void setRating(long rating) {
        this.rating = rating;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public void setVote(int vote) {
        this.vote = vote;
    }
    
    public int getVote() {
        return vote;
    }
    
    public long getAge() {
        return age;
    }

    public void setAge(long age) {
        this.age = age;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.parentId)
                .append(this.title)
                .append(this.text)
                .append(this.id)
                .append(this.created)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Gift other = (Gift) obj;
        return new EqualsBuilder()
                .append(this.parentId, other.parentId)
                .append(this.title, other.title)
                .append(this.text, other.text)
                .append(this.id, other.id)
                .append(this.created, other.created)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", this.id)
                .append("parentId", this.parentId)
                .append("title", this.title)
                .append("text", this.text)
                .append("created", this.created)
                .toString();
    }
}

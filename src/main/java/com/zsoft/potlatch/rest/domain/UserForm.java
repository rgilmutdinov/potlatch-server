package com.zsoft.potlatch.rest.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.core.style.ToStringCreator;

import com.zsoft.potlatch.domain.UserDao;

public class UserForm {
    private String username;
    private String email;
    private String password;
    
    public static UserDao toUser(UserForm userForm) {
        return UserDao.createUser(
                userForm.username,
                userForm.password,
                userForm.email);
    }
    
    public UserForm() {
        
    }
    
    public UserForm(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
    
    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.username)
                .append(this.email)
                .append(this.password)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UserForm other = (UserForm) obj;
        return new EqualsBuilder()
                .append(this.username, other.username)
                .append(this.email, other.email)
                .append(this.password, other.password)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("username", this.username)
                .append("email", this.email)
                .toString();
    }
}

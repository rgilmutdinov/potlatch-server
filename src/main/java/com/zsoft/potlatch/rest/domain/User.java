package com.zsoft.potlatch.rest.domain;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.core.style.ToStringCreator;

import com.zsoft.potlatch.domain.UserDao;
import org.apache.commons.codec.digest.DigestUtils;

public class User {
    private final static String GRAVATAR_URL = "http://www.gravatar.com/avatar/";
    
    private Long id;
    
    private String username;
    private String email;
    private String gravatarUrl;
    private Long rating = 0L;
    private Long giftsPosted = 0L;    
    
    public static User fromUser(UserDao user) {
        return new User(user);
    }   
    
    public User() {
        
    }
    
    public User(UserDao user) {
        this(user.getId(), user.getUsername(), user.getEmail());
        rating = user.getRating();
        giftsPosted = user.getGiftsPosted();
    }

    public User(Long id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
        
        if (email != null && !email.isEmpty()) {
            String emailHash = DigestUtils.md5Hex(email.toLowerCase().trim());
            this.gravatarUrl = GRAVATAR_URL + emailHash + ".jpg";
        }
    }
    
    public String getUsername() {
        return username;
    }

    public Long getId() {
        return id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }    

    public Long getGiftsPosted() {
        return giftsPosted;
    }

    public void setGiftsPosted(Long giftsPosted) {
        this.giftsPosted = giftsPosted;
    }

    public String getGravatarUrl() {
        return gravatarUrl;
    }

    public void setGravatarUrl(String gravatarUrl) {
        this.gravatarUrl = gravatarUrl;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.id)
                .append(this.username)
                .append(this.email)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        return new EqualsBuilder()
                .append(this.id, other.id)
                .append(this.username, other.username)
                .append(this.email, other.email)
                .isEquals();
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", this.id)
                .append("username", this.username)
                .append("email", this.email)
                .toString();
    }
}

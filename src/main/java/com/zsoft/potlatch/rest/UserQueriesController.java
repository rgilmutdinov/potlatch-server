package com.zsoft.potlatch.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Splitter;
import com.zsoft.potlatch.domain.UserDao;
import com.zsoft.potlatch.rest.domain.User;
import com.zsoft.potlatch.service.UserService;
import com.zsoft.potlatch.utils.SortUtils;

@RestController
@RequestMapping("/users")
public class UserQueriesController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/me")
    @ResponseStatus(HttpStatus.OK)
    public User aboutMe(Authentication authentication) {
        UserDao principal = (UserDao) authentication.getPrincipal();
        
        // we have to reload user profile from database
        UserDao me = userService.findById(principal.getId());
        if (me == null) {
            return User.fromUser(principal);
        }
        return User.fromUser(me);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<User> getAllUsers(
            @RequestParam(value = "s", defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d", defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p", defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l", defaultValue = "10",   required = false) Integer size) {

        List<User> users = new ArrayList<>();
        Sort sort = SortUtils.getSort(sortField, direction);
        PageRequest pageRequest = new PageRequest(page, size, sort);
        for (UserDao userDao : userService.getAllUsers(pageRequest)) {
            User user = User.fromUser(userDao);
            user.setEmail(""); // hide private data
            users.add(user);
        }        
        
        return users;
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/search")
    @ResponseStatus(HttpStatus.OK)
    public List<User> searchUsers(
            @RequestParam("q") String query,
            @RequestParam(value = "s", defaultValue = "id",   required = false) String sortField,
            @RequestParam(value = "d", defaultValue = "desc", required = false) String direction,
            @RequestParam(value = "p", defaultValue = "0",    required = false) Integer page,
            @RequestParam(value = "l", defaultValue = "10",   required = false) Integer size) {
                
        List<User> users = new ArrayList<>();
        Sort sort = SortUtils.getSort(sortField, direction);
        PageRequest pageRequest = new PageRequest(page, size, sort);
        for (UserDao userDao : userService.searchUsers(query, pageRequest)) {
            User user = User.fromUser(userDao);
            user.setEmail(""); // hide private data
            users.add(user);
        }        
        
        return users;
    } 
    
    @RequestMapping(method = RequestMethod.GET, value = "/refresh/{ids}")
    @ResponseStatus(HttpStatus.OK)
    public List<User> updateUsersCounts(
            @PathVariable String ids) {
        List<User> users = new ArrayList<>();
        if (ids == null || ids.isEmpty()) {
            return users;
        }
        
        List<Long> idList = new ArrayList<>();
        for (String sid : Splitter.on(',').split(ids)) {
            try {
                Long id = Long.parseLong(sid);
                idList.add(id);
            } catch (Exception exc) {
                /* do nothing, just skip */
            }
        }
        
        if (idList.size() == 0) {
            return users;
        }
        
        List<UserDao> userDaos = userService.findByIds(idList);
        for (UserDao userDao : userDaos) {
            users.add(User.fromUser(userDao));
        }
        return users;
    }
}

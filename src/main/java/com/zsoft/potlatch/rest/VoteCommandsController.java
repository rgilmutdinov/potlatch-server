package com.zsoft.potlatch.rest;

import com.zsoft.potlatch.domain.GiftDao;
import com.zsoft.potlatch.domain.UserDao;
import com.zsoft.potlatch.domain.VoteDao;
import com.zsoft.potlatch.rest.domain.Gift;
import com.zsoft.potlatch.service.GiftService;
import com.zsoft.potlatch.service.UserService;
import com.zsoft.potlatch.service.VoteService;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gifts")
public class VoteCommandsController {
    @Autowired
    private GiftService giftService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private VoteService voteService;
    
    @RequestMapping(value = "/{id}/voteUp", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Gift> toggleVoteUp(
            @PathVariable("id") long id, 
            Authentication authentication) throws IOException  {
        if (!giftService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        UserDao user = (UserDao) authentication.getPrincipal();
        GiftDao gift = giftService.findOne(id);        
        
        UserDao giftOwner = gift.getOwner();
        int ratingUpdate = 0;
        int voteValue = VoteDao.VOTE_UP;
        
        // check if the user has already voted the gift
        VoteDao vote = voteService.findByGiftIdAndUserId(id, user.getId());
        if (vote != null) {
            switch (vote.getVote()) {
                case VoteDao.VOTE_UP: // already voted up
                    //cancel the vote
                    voteValue = VoteDao.VOTE_ABSENT;
                    voteService.delete(vote.getId());
                    ratingUpdate = -1; // switch rating from +1 to 0
                    break;
                case VoteDao.VOTE_DOWN:
                    // switch negative vote to positive vote
                    vote.setVote(VoteDao.VOTE_UP);
                    voteService.save(vote);
                    ratingUpdate = 2; // switch rating from -1 to +1
                    break;
            }           
        } else {
            // user did not vote the gift
            vote = VoteDao.createVoteUp(gift, user.getId());
            voteService.save(vote);
            ratingUpdate = 1;
        }
        
        // update gift rating              
        gift.setRating(gift.getRating() + ratingUpdate);
        
        // update owner overall rating
        giftOwner.setRating(giftOwner.getRating() + ratingUpdate);       
        
        GiftDao savedGift = giftService.save(gift);
        userService.save(giftOwner);
        
        Gift giftResponse = Gift.fromGift(savedGift);
        
        giftResponse.setVote(voteValue);
        return new ResponseEntity<>(giftResponse, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/voteDown", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Gift> toggleVoteDown(
            @PathVariable("id") long id, 
            Authentication authentication) throws IOException {
        if (!giftService.exists(id)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        UserDao user = (UserDao) authentication.getPrincipal();
        GiftDao gift = giftService.findOne(id);        
        
        UserDao giftOwner = gift.getOwner();
        int ratingUpdate = 0;
        int voteValue = VoteDao.VOTE_DOWN;
        
        // check if the user has already voted the gift
        VoteDao vote = voteService.findByGiftIdAndUserId(id, user.getId());
        if (vote != null) {
            switch (vote.getVote()) {
                case VoteDao.VOTE_UP: 
                    // switch positive vote to negative vote
                    vote.setVote(VoteDao.VOTE_DOWN);
                    voteService.save(vote);
                    ratingUpdate = -2; // switch rating from +1 to -1
                    break;
                case VoteDao.VOTE_DOWN: // already voted down
                    //cancel the vote
                    voteValue = VoteDao.VOTE_ABSENT;
                    voteService.delete(vote.getId());
                    ratingUpdate = 1; // switch rating from 0 to +1
                    break;
            }           
        } else {
            // user did not vote the gift
            vote = VoteDao.createVoteDown(gift, user.getId());
            voteService.save(vote);
            ratingUpdate = -1;
        }
        
        // update gift rating              
        gift.setRating(gift.getRating() + ratingUpdate);
        
        // update owner overall rating
        giftOwner.setRating(giftOwner.getRating() + ratingUpdate);       
        
        GiftDao savedGift = giftService.save(gift);
        userService.save(giftOwner);
        
        Gift giftResponse = Gift.fromGift(savedGift);
        
        giftResponse.setVote(voteValue);
        return new ResponseEntity<>(giftResponse, HttpStatus.OK);
    }
}

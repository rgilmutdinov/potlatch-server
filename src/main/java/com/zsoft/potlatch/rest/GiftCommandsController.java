package com.zsoft.potlatch.rest;

import java.io.IOException;
import java.io.InputStream;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.zsoft.potlatch.config.AmazonS3Resources;
import com.zsoft.potlatch.domain.GiftDao;
import com.zsoft.potlatch.domain.UserDao;

import org.springframework.beans.factory.annotation.Autowired;

import com.zsoft.potlatch.rest.domain.GiftForm;
import com.zsoft.potlatch.rest.domain.Gift;
import com.zsoft.potlatch.service.GiftService;
import com.zsoft.potlatch.service.UserService;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/gifts")
public class GiftCommandsController {
    @Autowired
    private GiftService giftService;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private AmazonS3Resources amazonS3Resources;
    
    // delete specified gift
    // method deletes gift and all it's chained gifts from the database
    // it does not update ratings and amounts of posted gifts 
    // of affected users to keep things simple ;)
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deleteGift(
            @PathVariable Long id,
            Authentication authentication) {
        UserDao user = (UserDao) authentication.getPrincipal();
        GiftDao gift = giftService.findOne(id);
        if (gift == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        
        UserDao owner = gift.getOwner();
        if (user.getId() != owner.getId()) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        
        for (GiftDao chainedGift : giftService.findByParentId(gift.getId())) {
            amazonS3Resources.deleteImage(chainedGift.getUrl());
        }
        amazonS3Resources.deleteImage(gift.getUrl());
        
        giftService.delete(id);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Gift> createGift(
            @RequestPart("giftForm") GiftForm giftForm, 
            @RequestParam("imageFile") MultipartFile imageFile,
            UriComponentsBuilder builder,
            Authentication authentication) throws IOException {
        
        if (imageFile.isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        
        UserDao user = (UserDao) authentication.getPrincipal();

        GiftDao newGift = GiftForm.toGift(user, giftForm);
        
        try (InputStream input = imageFile.getInputStream()) {
            ObjectMetadata objectMetadata = new ObjectMetadata();
            objectMetadata.setContentLength(imageFile.getSize());
            objectMetadata.setContentType(imageFile.getContentType());
            
            String imageUrl = amazonS3Resources.uploadImage(
                    input, objectMetadata, imageFile.getOriginalFilename());
            
            newGift.setUrl(imageUrl);
        }
        
        GiftDao savedGift = giftService.save(newGift);
        
        // increase number of gifts posted
        user.setGiftsPosted(user.getGiftsPosted() + 1);
        userService.save(user);
        
        Gift giftResponse = Gift.fromGift(savedGift);
        
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(
                builder.path("/gifts/{id}")
                        .buildAndExpand(savedGift.getId().toString()).toUri());
        
        return new ResponseEntity<>(giftResponse, headers, HttpStatus.CREATED);
    }       
}
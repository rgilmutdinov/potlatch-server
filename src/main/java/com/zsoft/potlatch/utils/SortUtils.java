package com.zsoft.potlatch.utils;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

public class SortUtils {
    public static Sort getSort(String sortField, String direction) {
        Sort.Direction sortDirection = "desc".equalsIgnoreCase(direction)
                ? Sort.Direction.DESC 
                : Sort.Direction.ASC;
        
        if ("id".equalsIgnoreCase(sortField)) {
            return new Sort(sortDirection, "id");
        }
        return new Sort(
                new Order(sortDirection, sortField),
                new Order(Sort.Direction.DESC, "id"));
    }
}

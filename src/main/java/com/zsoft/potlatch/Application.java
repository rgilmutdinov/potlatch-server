package com.zsoft.potlatch;

import javax.servlet.MultipartConfigElement;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.MultipartProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.zsoft.potlatch.config.AwsConfig;
import com.zsoft.potlatch.config.OAuthConfig;
import com.zsoft.potlatch.config.PersistenceConfig;
import com.zsoft.potlatch.json.ResourcesMapper;

import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@EnableAutoConfiguration
@Configuration
@ComponentScan
@Import({OAuthConfig.class, AwsConfig.class, PersistenceConfig.class})
public class Application extends RepositoryRestMvcConfiguration {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public ObjectMapper halObjectMapper() {
        return new ResourcesMapper();
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartProperties props = new MultipartProperties();
        props.setMaxFileSize("10MB");
        props.setMaxRequestSize("10MB");
        return props.createMultipartConfig();
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() {
        return new CommonsMultipartResolver();
    }
}

INSERT INTO app_user (email, username, password, registration_date, user_role, rating, gifts_posted) VALUES ('admin@admin.com', 'admin', 'admin', current_date, 'ROLE_ADMIN', 0, 12);
INSERT INTO app_user (email, username, password, registration_date, user_role, rating, gifts_posted) VALUES ('user@user.com',   'user',  'user',  current_date, 'ROLE_USER', 0, 3);

INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test1', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test2', 'test2 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (2, null, 'test3', 'test3 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (2, 1,    'test4', 'test4 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (2, null, 'test5', 'test5 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test6', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test7', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test8', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test9', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test10', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test11', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test12', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test13', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test14', 'test1 gift', current_date, 'test_url', 0, 0);
INSERT INTO gift (owner_id, parent_id, title, text, created, url, views, rating) VALUES (1, null, 'test15', 'test1 gift', current_date, 'test_url', 0, 0);

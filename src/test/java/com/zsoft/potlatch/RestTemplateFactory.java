package com.zsoft.potlatch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.InterceptingClientHttpRequestFactory;

public class RestTemplateFactory {
    public final static String ID            = "potlatch";
    public final static String CLIENT_ID     = "potlatch_client";
    public final static String CLIENT_SECRET = "0f0335131edb444686624d9e177483aa";        
    
    public static OAuth2RestTemplate createOAuth2RestTemplate(
            String baseUrl, String userName, String password) throws Exception {
        ResourceOwnerPasswordResourceDetails resource = new ResourceOwnerPasswordResourceDetails();

        resource.setAccessTokenUri(baseUrl + "/oauth/token");
        resource.setClientId(CLIENT_ID);
        resource.setId(ID);
        resource.setScope(Arrays.asList("write", "read"));
        resource.setClientSecret(CLIENT_SECRET);
        resource.setUsername(userName);
        resource.setPassword(password);  
        
        OAuth2RestTemplate template = new OAuth2RestTemplate(resource);
        disableCertificateChecks(template);
        enableTracing(template);
        
        return template;
    }
    
    public static RestTemplate createAnonymousRestTemplate(String baseUrl)
            throws Exception {
        RestTemplate template = new RestTemplate();

        SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(null,
                new TrustSelfSignedStrategy()).build();

        CloseableHttpClient httpClient = HttpClients
                .custom()
                .setSslcontext(sslContext)
                .setHostnameVerifier(new AllowAllHostnameVerifier()) // allow all hostnames
                .build();

        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                httpClient);
        template.setRequestFactory(requestFactory);
        
        enableTracing(template);
        
        return template;
    }
    
    private static void disableCertificateChecks(OAuth2RestTemplate oauthTemplate) throws Exception {
        SSLContext sslContext = new SSLContextBuilder()
                .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                .build();
        
        CloseableHttpClient httpClient = HttpClients
                .custom()
                .setSslcontext(sslContext)
                .setHostnameVerifier(new AllowAllHostnameVerifier()) // allow all hostnames
                .build();
        
        ClientHttpRequestFactory requestFactory = 
                new HttpComponentsClientHttpRequestFactory(httpClient);        

        // This is for OAuth protected resources
        oauthTemplate.setRequestFactory(requestFactory);

        // ResourceOwnerPasswordAccessTokenProvider creates it's own 
        // RestTemplate for token operations
        ResourceOwnerPasswordAccessTokenProvider provider =
                new ResourceOwnerPasswordAccessTokenProvider();
        
        provider.setRequestFactory(requestFactory);        
        oauthTemplate.setAccessTokenProvider(provider);
    }
    
    private static void enableTracing(RestTemplate restTemplate) {
        ClientHttpRequestFactory requestFactory = restTemplate.getRequestFactory();
        List<ClientHttpRequestInterceptor> ris = new ArrayList<ClientHttpRequestInterceptor>();
        ris.add(new LoggingRequestInterceptor());

        restTemplate.setInterceptors(ris);
        restTemplate.setRequestFactory(new InterceptingClientHttpRequestFactory(
                requestFactory , ris));
    }
}
package com.zsoft.potlatch;

import com.google.common.base.Joiner;
import com.zsoft.potlatch.api.PotlatchUrls;
import com.zsoft.potlatch.data.TestData;
import com.zsoft.potlatch.rest.domain.GiftForm;
import com.zsoft.potlatch.rest.domain.Gift;
import com.zsoft.potlatch.rest.domain.User;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest("server.port:0") 
@ActiveProfiles("test")
public class ApplicationTests {
    public final static String USER_NAME     = "admin";
    public final static String USER_PASSWORD = "admin";
    
    @Value("${keystore.file:src/test/resources/private/cloud.png}")
    private String testImageFileName;
    
    @Value("${local.server.port}")
    private int port;
    
    private PotlatchUrls urls;
    
    @Before
    public void setUp() {
        urls = new PotlatchUrls(port);
    }   
    
    @Test
    public void testPagination() throws Exception {
        RestTemplate restTemplate = createRestTemplate();
        
        int page = 0;
        int limit = 10;
        
        ResponseEntity<Gift[]> entity = 
                restTemplate.getForEntity(urls.getGiftsPageUrl(), Gift[].class, page, limit);
        Gift[] gifts = entity.getBody();   
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(10, gifts.length);
        
        page++;
        entity = restTemplate.getForEntity(urls.getGiftsPageUrl(), Gift[].class, page, limit);
        gifts = entity.getBody();
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(5, gifts.length);
    }
    
    @Test
    public void testListGifts() throws Exception {
        RestTemplate restTemplate = createRestTemplate();

        ResponseEntity<Gift[]> entity
                = restTemplate.getForEntity(urls.getGiftsUrl(), Gift[].class);

        Gift[] gifts = entity.getBody();

        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertTrue(gifts.length > 0);
    }

    @Test
    public void testListMyGifts() throws Exception {
        RestTemplate restTemplate = createRestTemplate();

        ResponseEntity<Gift[]> entity
                = restTemplate.getForEntity(urls.getMyGiftsUrl(), Gift[].class);

        Gift[] gifts = entity.getBody();

        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertTrue(gifts.length > 0);
    }
    
    @Test
    public void testGiftNotExists() throws Exception {
        RestTemplate restTemplate = createRestTemplate();
                
        try {
            ResponseEntity<Gift> giftEntity = restTemplate.getForEntity(urls.getGiftUrl(99999L), Gift.class);

            fail("Request Passed incorrectly with status "
                    + giftEntity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.NOT_FOUND, ex.getStatusCode());
        }
    }
    
    @Test 
    public void testUpdateUserCounts() throws Exception {
        RestTemplate restTemplate = createRestTemplate();
        
        ResponseEntity<User> userEntity;
        
        userEntity = restTemplate.getForEntity(urls.getMyProfileUrl(), User.class);
        assertEquals(HttpStatus.OK, userEntity.getStatusCode());
        User me = userEntity.getBody();
        assertNotNull(me);
        long initialRating = me.getRating();
        
        ResponseEntity<Gift[]> myGiftsEntity = restTemplate.getForEntity(urls.getMyGiftsUrl(), Gift[].class);
        Gift[] gifts = myGiftsEntity.getBody();

        // vote up for own gift
        ResponseEntity<Gift> giftEntity = restTemplate.postForEntity(urls.getGiftVoteUpUrl(gifts[0].getId()), null, Gift.class);   
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        
        userEntity = restTemplate.getForEntity(urls.getMyProfileUrl(), User.class);
        assertEquals(HttpStatus.OK, userEntity.getStatusCode());
        me = userEntity.getBody();
        assertNotNull(me);
        assertEquals(initialRating + 1, me.getRating().longValue());
        
        List<Long> ids = new ArrayList<>();
        ids.add(me.getId());
        ids.add(9999999L);
        
        String sids = Joiner.on(',').join(ids);
        String url = urls.getUpdateUsersCountUrl() + "/" + sids;
        User[] updatedUsers =
                restTemplate.getForObject(url, User[].class);
        assertEquals(1, updatedUsers.length);
        assertEquals(updatedUsers[0].getRating().longValue(), initialRating + 1);
    }
    
    @Test 
    public void testUpdateGiftCounts() throws Exception {
        RestTemplate restTemplate = createRestTemplate();
        
        Long id = 2L;
        
        ResponseEntity<Gift> voteGiftEntity;
        ResponseEntity<Gift> giftEntity;
        
        giftEntity = restTemplate.getForEntity(urls.getGiftUrl(id), Gift.class);
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        assertNotNull(giftEntity.getBody());
        long initialRating = giftEntity.getBody().getRating();
        
        // vote up
        voteGiftEntity = restTemplate.postForEntity(urls.getGiftVoteUpUrl(id), null, Gift.class);   
        assertEquals(HttpStatus.OK, voteGiftEntity.getStatusCode());
        
        giftEntity = restTemplate.getForEntity(urls.getGiftUrl(id), Gift.class);
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        assertNotNull(giftEntity.getBody());
        assertEquals(initialRating + 1, giftEntity.getBody().getRating());
        
        List<Long> ids = new ArrayList<>();
        ids.add(id);
        ids.add(9999999L);
        
        String sids = Joiner.on(',').join(ids);
        String url = urls.getUpdateCountUrl() + "/" + sids;
        Gift[] updatedGifts =
                restTemplate.getForObject(url, Gift[].class);
        assertEquals(1, updatedGifts.length);
        assertEquals(updatedGifts[0].getRating(), initialRating + 1);
    }
    
    @Test 
    public void testGiftVoting() throws Exception {
        RestTemplate restTemplate = createRestTemplate();
        Long id = 1L;
        
        ResponseEntity<Gift> voteGiftEntity;
        ResponseEntity<Gift> giftEntity;
        
        // vote up
        voteGiftEntity = restTemplate.postForEntity(urls.getGiftVoteUpUrl(id), null, Gift.class);   
        assertEquals(HttpStatus.OK, voteGiftEntity.getStatusCode());
        
        giftEntity = restTemplate.getForEntity(urls.getGiftUrl(id), Gift.class);
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        assertNotNull(giftEntity.getBody());
        assertEquals(1, giftEntity.getBody().getRating());
        
        // vote down
        voteGiftEntity = restTemplate.postForEntity(urls.getGiftVoteDownUrl(id), null, Gift.class);
        assertEquals(HttpStatus.OK, voteGiftEntity.getStatusCode());
        
        giftEntity = restTemplate.getForEntity(urls.getGiftUrl(id), Gift.class);
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        assertNotNull(giftEntity.getBody());
        assertEquals(-1, giftEntity.getBody().getRating());
        
        // vote down again
        voteGiftEntity = restTemplate.postForEntity(urls.getGiftVoteDownUrl(id), null, Gift.class);
        assertEquals(HttpStatus.OK, voteGiftEntity.getStatusCode());
        
        giftEntity = restTemplate.getForEntity(urls.getGiftUrl(id), Gift.class);
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        assertNotNull(giftEntity.getBody());
        assertEquals(0, giftEntity.getBody().getRating());
    }
    
    @Test 
    public void testGiftToggleVoteUp() throws Exception {
        RestTemplate restTemplate = createRestTemplate();
        Long id = 3L;
        
        ResponseEntity<Gift> voteGiftEntity;
        ResponseEntity<Gift> giftEntity;
        
        // vote up
        voteGiftEntity = restTemplate.postForEntity(urls.getGiftVoteUpUrl(id), null, Gift.class);   
        assertEquals(HttpStatus.OK, voteGiftEntity.getStatusCode());
        
        giftEntity = restTemplate.getForEntity(urls.getGiftUrl(id), Gift.class);
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        assertNotNull(giftEntity.getBody());
        assertEquals(1, giftEntity.getBody().getRating());
        
        // vote up again
        voteGiftEntity = restTemplate.postForEntity(urls.getGiftVoteUpUrl(id), null, Gift.class);
        assertEquals(HttpStatus.OK, voteGiftEntity.getStatusCode());
        
        giftEntity = restTemplate.getForEntity(urls.getGiftUrl(id), Gift.class);
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        assertNotNull(giftEntity.getBody());
        assertEquals(0, giftEntity.getBody().getRating());
    }
    
    @Test
    public void testGetGiftsOfUser() throws Exception {
        RestTemplate restTemplate = createRestTemplate();

        Long ownerId = 1L;
        ResponseEntity<Gift[]> entity
                = restTemplate.getForEntity(urls.getGiftsByOwnerIdUrl(ownerId), 
                        Gift[].class, ownerId);

        Gift[] gifts = entity.getBody();

        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertTrue(gifts.length > 0);
    }
    
    @Test
    public void testGetChildGifts() throws Exception {
        RestTemplate restTemplate = createRestTemplate();

        Long parentId = 1L;
        ResponseEntity<Gift[]> entity
                = restTemplate.getForEntity(urls.getGiftsByParentIdUrl(parentId), 
                        Gift[].class, parentId);

        Gift[] childGifts = entity.getBody();

        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(1, childGifts.length);
    }
    
    @Test
    public void testSearchGifts() throws Exception {
        RestTemplate restTemplate = createRestTemplate();
        
        String query = "1";
        int page = 0;
        int limit = 10;
        
        ResponseEntity<Gift[]> entity = 
                restTemplate.getForEntity(urls.getSearchGiftsUrl(), Gift[].class, query, page, limit);
        Gift[] gifts = entity.getBody();   
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertTrue(gifts.length > 0);
    }
    
    @Test
    public void testSearchChainedGifts() throws Exception {
        RestTemplate restTemplate = createRestTemplate();
        
        Long parentId = 1L;
        String query = "4";
        int page = 0;
        int limit = 10;
        
        ResponseEntity<Gift[]> entity = 
                restTemplate.getForEntity(urls.getSearchChainedGiftsUrl(parentId), 
                        Gift[].class, query, page, limit);
        Gift[] gifts = entity.getBody();   
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertTrue(gifts.length > 0);
        
        query = "title that no exists";
        entity = restTemplate.getForEntity(urls.getSearchChainedGiftsUrl(parentId), 
                Gift[].class, query, page, limit);
        gifts = entity.getBody();   
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(0, gifts.length);
    }
    
    @Test
    public void testGetChildGiftsNotExist() throws Exception {
        RestTemplate restTemplate = createRestTemplate();

        Long parentId = 999999L;
        ResponseEntity<Gift[]> entity
                = restTemplate.getForEntity(urls.getGiftsByParentIdUrl(parentId), 
                        Gift[].class, parentId);

        Gift[] childGifts = entity.getBody();

        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(0, childGifts.length);
    }
    
    @Test
    public void testListGiftsForAnonymous() throws Exception {
        RestTemplate restTemplate = 
                RestTemplateFactory.createAnonymousRestTemplate(urls.getBaseUrl());

        ResponseEntity<Gift[]> entity
                = restTemplate.getForEntity(urls.getGiftsUrl(), Gift[].class);

        Gift[] gifts = entity.getBody();

        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertTrue(gifts.length > 0);
    }
    
    @Test
    public void testAddAndGetGift() throws Exception {
        RestTemplate restTemplate = createRestTemplate();

        GiftForm giftForm = TestData.randomGift();
        
        final String testImageFile = new File(testImageFileName).getAbsolutePath();
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("imageFile", new FileSystemResource(testImageFile));
        parts.add("giftForm", giftForm);

        ResponseEntity<Gift> giftEntity = 
                restTemplate.postForEntity(urls.getGiftsUrl(), parts, Gift.class);

        Gift savedGift = giftEntity.getBody();

        assertEquals(HttpStatus.CREATED, giftEntity.getStatusCode());
        assertNotNull(savedGift);
        assertNotNull(savedGift.getId());
        assertEquals(savedGift.getParentId(), giftForm.getParentId());
        assertEquals(savedGift.getTitle(), giftForm.getTitle());
        assertEquals(savedGift.getText(), giftForm.getText());
        
        giftEntity = restTemplate.getForEntity(urls.getGiftUrl(savedGift.getId()), Gift.class);
        
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        assertEquals(savedGift, giftEntity.getBody());
        assertNotNull(giftEntity.getBody().getUrl());
    }
    
    @Test
    public void testPostGetGiftWithoutImage() throws Exception {
        RestTemplate restTemplate = createRestTemplate();

        GiftForm giftForm = TestData.randomGift();
                
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();        
        parts.add("giftEntry", giftForm);

        try {
            ResponseEntity<Gift> giftEntity
                = restTemplate.postForEntity(urls.getGiftsUrl(), parts, Gift.class);       
            
            fail("Request Passed incorrectly with status "
                    + giftEntity.getStatusCode());
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.BAD_REQUEST, ex.getStatusCode());
        }
    }
    
    private RestTemplate createRestTemplate() throws Exception {
        return RestTemplateFactory.createOAuth2RestTemplate(
                urls.getBaseUrl(), USER_NAME, USER_PASSWORD); 
    }
    
    private HttpHeaders defaultHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

        return headers;
    }
}
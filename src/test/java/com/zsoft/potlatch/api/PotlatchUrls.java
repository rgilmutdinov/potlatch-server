package com.zsoft.potlatch.api;

import java.net.URI;
import java.text.MessageFormat;

public class PotlatchUrls {    
    private final String baseUrl;
    
    public PotlatchUrls(int port) {        
        baseUrl = MessageFormat.format("https://localhost:{0}", Integer.toString(port));
    }
    
    public String getBaseUrl() {
        return baseUrl;
    }
    
    public String getGiftsUrl() {
        return baseUrl + "/gifts";
    }
    
    public String getGiftsPageUrl() {
        return baseUrl + "/gifts?p={p}&l={l}";
    }
    
    public String getGiftUrl(Long id) {
        return MessageFormat.format(getGiftsUrl() + "/{0}", Long.toString(id));
    }
    
    public String getGiftsByOwnerIdUrl(Long ownerId) {
        return MessageFormat.format(getBaseUrl() + "/users/{0}/gifts", Long.toString(ownerId));
    }
    
    public String getMyGiftsUrl() {
        return getBaseUrl() + "/users/me/gifts";
    }
    
    public String getGiftsByParentIdUrl(Long parentId) {
        return MessageFormat.format(getGiftsUrl() + "/{0}/chain", Long.toString(parentId));
    }
    
    public String getSearchGiftsUrl() {
        return getGiftsUrl() + "/search?q={q}&p={p}&l={l}";
    }
    
    public String getSearchChainedGiftsUrl(Long parentId) {
        return MessageFormat.format(getGiftsUrl() + "/{0}", Long.toString(parentId)) + "/search?q={q}&p={p}&l={l}";
    }
    
    public String getChainedGiftsUrl(Long id) {
        return MessageFormat.format(getGiftsUrl() + "/{0}", Long.toString(id));
    }
    
    public String getGiftVoteUpUrl(Long id) {
        return getGiftUrl(id) + "/voteUp";
    }
    
    public String getGiftVoteDownUrl(Long id) {
        return getGiftUrl(id) + "/voteDown";
    }

    public String getUpdateCountUrl() {
        return getGiftsUrl() + "/refresh";
    }

    public String getUpdateUsersCountUrl() {
        return getBaseUrl() + "/users/refresh";
    }
    
    public String getMyProfileUrl() {
        return getBaseUrl() + "/users/me";
    }
}

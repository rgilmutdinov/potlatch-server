package com.zsoft.potlatch.rest.functional;

import com.zsoft.potlatch.RestTemplateFactory;
import com.zsoft.potlatch.api.PotlatchUrls;
import com.zsoft.potlatch.data.TestData;
import com.zsoft.potlatch.rest.domain.GiftForm;
import com.zsoft.potlatch.rest.domain.Gift;
import java.io.File;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class GiftTests {
    private int port = 8080;
    
    private PotlatchUrls urls;
    
    @Before
    public void setUp() {
        urls = new PotlatchUrls(port);
    }
    
    /*
    @Test
    public void testAddAndGetGift() throws Exception {
        RestTemplate restTemplate = RestTemplateFactory.createOAuth2RestTemplate(
                urls.getBaseUrl(), "user", "user"); 

        GiftForm giftForm = TestData.randomGift();
        
        final String testImageFile = new File("C:\\tempfiles\\test.PNG").getAbsolutePath();
        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<>();
        parts.add("imageFile", new FileSystemResource(testImageFile));
        parts.add("giftForm", giftForm);

        ResponseEntity<Gift> giftEntity = 
                restTemplate.postForEntity(urls.getGiftsUrl(), parts, Gift.class);

        Gift savedGift = giftEntity.getBody();

        assertEquals(HttpStatus.CREATED, giftEntity.getStatusCode());
        assertNotNull(savedGift);
        assertNotNull(savedGift.getId());
        assertEquals(savedGift.getParentId(), giftForm.getParentId());
        assertEquals(savedGift.getTitle(), giftForm.getTitle());
        assertEquals(savedGift.getText(), giftForm.getText());
        
        giftEntity = restTemplate.getForEntity(urls.getGiftUrl(savedGift.getId()), Gift.class);
        
        assertEquals(HttpStatus.OK, giftEntity.getStatusCode());
        assertEquals(savedGift, giftEntity.getBody());
        assertNotNull(giftEntity.getBody().getUrl());
    }

    @Test
    public void testPagination() throws Exception {
        RestTemplate restTemplate = RestTemplateFactory.createOAuth2RestTemplate(
                urls.getBaseUrl(), "user", "user"); 
        
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();        
        params.add("page", 1);
        params.add("limit", 10);
        ResponseEntity<Gift[]> entity = 
                restTemplate.getForEntity(urls.getGiftsUrl(), Gift[].class, params);

        Gift[] gifts = entity.getBody();
        
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(10, gifts.length);
    }
    */
}

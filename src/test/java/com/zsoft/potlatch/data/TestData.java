package com.zsoft.potlatch.data;

import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zsoft.potlatch.rest.domain.GiftForm;

public class TestData {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static GiftForm randomGift() {		
        String id = UUID.randomUUID().toString();
        String title = "Gift-" + id;
        String text  = "test gift";        
        return new GiftForm(null, title, text);
    }

    public static String toJson(Object o) throws Exception {
        return objectMapper.writeValueAsString(o);
    }
}
